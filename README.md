# schedule-maker
A command-line utility to create your school schedules.

Creates an .xlsx file with your school schedules from a .yaml file with your classes.

```
Schedule-maker - a command-line utility to create your school schedules.

Usage: schedule-maker-exe FILENAME [-p|--pretty-print] [-o|--output FILENAME]

  Create an .xlsx file with your school schedules from a .yaml file with your
  classes.

Available options:
  FILENAME                 YAML input file
  -p,--pretty-print        Print to stdout the validated schedules
  -o,--output FILENAME     Write output to FILE (.xlsx)
                           (default: "schedules.xlsx")
  -h,--help                Show this help text

still a work in progress, source code here:
https://codeberg.org/0rphee/schedule-maker.
```
